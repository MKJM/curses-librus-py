#imports

import curses
import time

class Application:
    def __init__(self):
        
        self.win_parent = None
        self.win_top = None
        self.win_tree = None
        self.screen = None
        self.menu = None
        self.y = None
        self.x = None

        #init_terminal(self)
        #init_windows(self)

    def exit_application(self):

        curses.nocbreak()
        self.screen.keypad(False)
        curses.echo()
        curses.endwin()

    def get_screen_size(self):
        self.y = curses.LINES
        self.x = curses.COLS

    def init_windows(self):

        self.win_parent = curses.newwin(0,0,0,0) #fullscreen
        #initialize top-view window
        #subwindow (20% of screen from top to bottom)
        self.win_top = self.win_parent.subwin(int(0.2*self.y), self.x, 0, 0)

        #initialize tree-view window 
        # 80% vertical space, 30% horizontal, bottom-left
        self.win_tree = self.win_parent.subwin(int(0.8*self.y), int(0.3*self.x), int(0.2*self.y), 0)
        
        #initialize details window
        self.win_details = self.win_arent.subwin(int(0.8*self.y), int(0.7*self.x), int(0.2*self.y), int(0.3*self.x))

        #window borders /w curses boxes
        VCHAR = curses.ACS_VLINE
        HCHAR = curses.ACS_HLINE

        win_top.box(VCHAR, HCHAR)
        win_tree.box(VCHAR, HCHAR)
        win_details.box(VCHAR, HCHAR)
        win_parent.refresh() #refreshing parent window refreshes all subwins

    def init_terminal(self):

        stdscr = curses.initscr()
        curses.start_color()
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
        curses.noecho()
        curses.cbreak()
        stdscr.keypad(True)
        stdscr.clear()
        self.screen = stdscr 


    #def tree_handler():

    #def nav_bar_handler():

    #def menu_handler():

    #def key_handler():

#main loop
    

