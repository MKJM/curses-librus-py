import curses
import time

#initialize objects ( global )

win_parent = None
win_top = None
win_tree = None
win_details = None
screen = None

menu = [['Option1', 'Suboption1', 'Suboption2'], ['Option2'], ['Option3']]
global_row = 0
global_col = 0
submenu_row = 0

def exit_cli(screen):
    curses.nocbreak()
    screen.keypad(False)
    curses.echo()
    curses.endwin()

def initialize_cli():
    stdscr = curses.initscr()
    curses.start_color() #allows for text color manipulation
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE) #for highlighting

    curses.noecho()
    curses.cbreak()
    stdscr.keypad(True)
    stdscr.clear()
    return stdscr

def get_screen_size():
    y = curses.LINES
    x = curses.COLS
    return y, x

def initialize_windows(y, x):

    #Initialize parent window
    global win_parent
    win_parent = curses.newwin(0, 0, 0, 0) #fullscreen window

    #initialize top-view window
    #subwindow (20% of screen from top to bottom)
    global win_top
    win_top = win_parent.subwin(int(0.2*y), x, 0, 0)

    #initialize tree-view window
    # 80% vertical space, 30% horizontal, bottom-left
    global win_tree
    win_tree = win_parent.subwin(int(0.8*y), int(0.3*x), int(0.2*y), 0)

    #initialize details window
    #70% of horizontal space and 80% of vertical space, bottom-right
    global win_details
    win_details = win_parent.subwin(int(0.8*y), int(0.7*x), int(0.2*y), int(0.3*x))

    #initialize window borders for all subwindows

    VCHAR = ord("|")
    HCHAR = curses.ACS_HLINE    #set the characters used for drawing borders

    win_top.box(VCHAR, HCHAR)

    win_tree.box(VCHAR, HCHAR)

    win_details.box(VCHAR, HCHAR)

    win_parent.refresh() #refreshing parent window refreshes all subwindows

def populate_top(window):

    window.addstr(1, 1, "Hello World")
    window.addstr(7, 1, "Lucky Number: {}".format(get_lucky()))
    window.refresh()
def populate_tree(window):
    
    menu_index, submenu_index = 0, 0
    window.addstr(1, 1, "Menu Option1")
    window.addstr(2, 1, "Menu Option2")
    window.addstr(3, 3, "Submenu Option1")
    window.addstr(4, 3, "Submenu Option2")
    window.refresh()
    
def get_lucky():

    lucky_number = 14
    return lucky_number
def tree_handler(key, row, col):
    
    global global_row
    row = global_row
        
    global global_col
    col = global_col
    
    global submenu_row
    
    if key == ord("h"):
        
        #makes the columns not jump back to the beginning/end
        if col == 0:
           pass 
        else:
            col -= 1
        
        #reset the currently selected row to 0 (when switching between cols)
        
    elif key == ord("j"):
        row += 1
        if col == 1:
            submenu_row += 1
    elif key == ord("k"):
        row -= 1
        if col == 1:
            submenu_row -= 1
    elif key == ord("l"):

        if col == 1:
            pass
        else:
            col += 1
    
    global_col = col % 2 
    if global_col == 0:
        global_row = row % len(menu)
    else:
        global_row = submenu_row % len(menu[row])

    win_details.addstr(1,1, "Active row: {}".format(global_row))
    win_details.addstr(2,1, "Active column: {}".format(global_col))
    win_details.refresh()
    win_parent.refresh()


    win_details.refresh()

    menu_handler(global_row, global_col, menu)    #handles menu highlighting based on current row and col

    return None

def menu_handler(selected_row, selected_col, menu):

    win_tree.clear() #clears the tree before writing to it
    h, w = win_tree.getmaxyx() #get window size
    '''
    #enumerate through menu items; highlight the currently chosen option
    for idx, row in enumerate(menu):
        x = w//2 
        y = h//2 + idx
        if idx==selected_row:
            win_tree.attron(curses.color_pair(1))
            win_tree.addstr(y, x, row)      #if curr selected, highligh it
            win_tree.attroff(curses.color_pair(1))
        else:
            win_tree.addstr(y, x, row)
    win_tree.refresh()
    
    for idx, option in enumerate(menu):
        x = w//2
        y = h//2+idx

        if len(option) > 1:
            #draw suboption
            for suboption in option[1:]:
                win_tree.addstr(y+option.index(suboption), x+5, suboption)
        else:
            if idx==selected_row:
                win_tree.addstr(y,x, option[0])
    win_tree.refresh()
    '''
    for option_index, option in enumerate(menu):

        for suboption_index, suboption in enumerate(option):
            
            x = w//2
            y = h//2 + option_index + suboption_index

            #option[0] is the option itself, not its suboption
            if suboption_index == 0:
                
                if option_index > 0:
                    y += len(menu[option_index-1]) #compensate for previously drawn suboptions 

                #check whether currently selected column and row
                if selected_col == 0:
                    pass            
                win_tree.addstr(y, x, suboption)
            else:
                x += 5  #submenu 5 chars to the right
                win_tree.addstr(y, x, suboption)
    win_tree.refresh()

def get_active_window(last_active, key):
    
    if key == ord("H"):  # Shift - h
        return 'tree'
    elif key == ord("L"):
        return 'details'
    elif last_active == 'tree' or last_active == 'details':
        return last_active
    else:
        return 'none'



def main():
     
    screen = initialize_cli()
    y, x = get_screen_size()
    initialize_windows(y, x)
    populate_top(win_top)
    populate_tree(win_tree)
     
    key = win_parent.getch()
    last_active = None
    tree_row, tree_col = 0, 0 #current row and col in the tree view

    #main loop
    while key != ord("q"):
        
        last_active = get_active_window(last_active, key)
        win_details.erase()
        if last_active == 'tree':
            win_details.addstr(int(0.7*y),1,"TREE")
            tree_handler(key, tree_row, tree_col)
            win_top.refresh()
        if last_active == 'details':
            win_details.addstr(int(0.7*y),1,"DETAILS") 
        win_details.refresh()
        key = win_parent.getch()
    #After main loop terminates, exit the program
    exit_cli(screen)
main()
